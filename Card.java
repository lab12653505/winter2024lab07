public class Card{
    String suit;
    String rank;

    public Card(String suit, String rank){
        this.suit = suit;
        this.rank = rank;
    }

    public String getSuit(){
        return this.suit;
    }
    
    public String getRank(){
        return this.rank;
    }
	
	public double calculateScore(){
		double cardValue = 0.0;
		
		//Adds rank
		if (rank.equals("Jack")){
			cardValue += 11.0;
		} else if (rank.equals("Queen")){
			cardValue += 12.0;
		} else if (rank.equals("King")){
			cardValue += 13.0;
		} else if (rank.equals("Ace")){
			cardValue += 1.0;
		} else {
			cardValue += Double.parseDouble(rank);
		}
		
		//Adds suit
		if (this.suit.equals("Hearts")){
			cardValue += 0.4;
		} else if (this.suit.equals("Spades")){
			cardValue += 0.3;
		} else if (this.suit.equals("Diamonds")){
			cardValue += 0.2;
		} else {
			cardValue += 0.1;
		}
		
		return cardValue;
	}

    public String toString() {
        return rank + " of " + suit;
    }
}