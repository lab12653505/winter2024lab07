public class SimpleWar{
	public static void main(String args[]){
		//Creating Deck object
		Deck stack = new Deck();
		//previous test: 
		//System.out.println(stack);
		stack.shuffle();
		//previous test: 
		//System.out.println(stack);
		
		//player point variables
		int pointOne = 0;
		int pointTwo = 0;
		
		//previous test
		while (stack.length()>1){
			Card One = stack.drawTopCard();
			double pOne = One.calculateScore();
			System.out.println( "p1:" + "\n" + pOne + "\n" + One + "\n");
			
			Card Two = stack.drawTopCard();
			double pTwo = Two.calculateScore();
			System.out.println( "p2:" + "\n" + pTwo + "\n" + Two + "\n");
			
			if (pOne == pTwo){
				System.out.println("tie");
			} else if (pOne > pTwo){
				System.out.println("p1 wins\n");
				pointOne++;
			} else {
				System.out.println("p2 wins\n");
				pointTwo++;
			}
			
			System.out.println("p1 points: " + pointOne + "\np2 points: " + pointTwo + "\n");
		}
		
		if (pointOne == pointTwo){
			System.out.println("its a tie!");
		} else if (pointOne > pointTwo){
			System.out.println("Player one Won!");
		} else {
			System.out.println("Player two Won!");
		}
	}
}