import java.util.Random;

public class Deck{
    Card[] cards;
    int numberOfCards;
    Random rng;

    public Deck(){
        this.rng = new Random();
        this.cards = new Card[52];
        this.numberOfCards = 52;
        
        String[] suits = new String[] {"Hearts", "Clubs", "Spades", "Diamonds"};
        String[] rank = new String[] {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
        int suitIndex = 0;
        int rankIndex = 0;

        for(int i = 0; i<this.cards.length;i++){
        //for(Cards i : this.card){
            if (rankIndex>rank.length-1){
                rankIndex = 0;
                suitIndex++;
            }
        //i = new Card...
            this.cards[i] = new Card(suits[suitIndex], rank[rankIndex]);
            rankIndex++;
        }
    }

    public int length(){
        return this.numberOfCards;
    }

    public Card drawTopCard(){
        numberOfCards--;
        return this.cards[numberOfCards];
    }

    public String toString() {
        String sum = "";
        for(int i = 0; i<numberOfCards;i++){
            sum+= cards[i].toString() + "\n";
        }
        return sum;
    }

    public void shuffle() {
        Card placeholder = new Card("", "");
        int randplace = 0;
        for(int i = 0; i<numberOfCards; i++){
            placeholder = cards[i];
            randplace = i + rng.nextInt(numberOfCards-i);
            cards[i] = cards[randplace];
            cards[randplace] = placeholder;
        }
    }
}